#include	"chip.h"
#include	"canpie.h"
#include	"lpc15xx_can.h"

void msgobj_to_cpmsg(CAN_MSG_OBJ *msgobj, _TsCpCanMsg *cpmsg)
{
	cpmsg->ubMsgCtrl = 0;

	// Get node ID and set ID type in msgCtrl
	if (msgobj->mode_id & CAN_MSGOBJ_EXT) {
		// Grab the 29-bit identifier
		_U32 id = msgobj->mode_id & ID_EXT_MASK;
		cpmsg->tuMsgId.ulExt = id;
		cpmsg->ubMsgCtrl |= CP_MASK_EXT_BIT;
	}
	else {
		_U16 id = (_U16) (msgobj->mode_id & ID_STD_MASK);
		cpmsg->tuMsgId.uwStd = id;
	}
		// Check if it's an RTR frame
	if ((msgobj->mode_id & CAN_MSGOBJ_RTR) == CAN_MSGOBJ_RTR) {
		// It's an RTR frame so set the control bit
		cpmsg->ubMsgCtrl |= CP_MASK_RTR_BIT;
	}

	// Data length code copy
	cpmsg->ubMsgDLC = msgobj->dlc;

	// Now copy the data
	for (int i = 0; i < 8; i++) {
		cpmsg->tuMsgData.aubByte[i] = msgobj->data[i];
	}
}

void cpmsg_to_msgobj(_TsCpCanMsg *cpmsg, CAN_MSG_OBJ *msgobj)
{
	if (cpmsg->ubMsgCtrl & 1) {
		// Extended frame
		msgobj->mode_id = CAN_MSGOBJ_EXT;
		msgobj->mode_id |= cpmsg->tuMsgId.ulExt;
	}
	else {
		// Standard frame
		msgobj->mode_id = CAN_MSGOBJ_STD;
		msgobj->mode_id |= cpmsg->tuMsgId.uwStd;
	}

	if (cpmsg->ubMsgCtrl & 2) {
		// RTR frame
		msgobj->mode_id |= CAN_MSGOBJ_RTR;
	}

	// Set DLC
	msgobj->dlc = cpmsg->ubMsgDLC;

	// Copy Data
	for (int i = 0; i < 8; i++) {
		msgobj->data[i] = cpmsg->tuMsgData.aubByte[i];
	}

	msgobj->mask = 0x0;
}


