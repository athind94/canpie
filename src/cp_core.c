/* ---------------------------------------------------------------------------
** This software is in the public domain, furnished "as is", without technical
** support, and with no warranty, express or implied, as to its usefulness for
** any purpose.
**
** cp_core.c
** Implementation of CANpie core function set
**
** Author: Anmol Thind
** -------------------------------------------------------------------------*/

#include	"chip.h"
#include	"cp_arch.h"
#include	"cp_core.h"
#include	"cp_msg.h"
#include	"canpie.h"
#include	"lpc15xx_can.h"
#include	"lpc_helper.h"

#define LOOPBACK_MODE

#define MAX_CAN_PARAM_SIZE          512
static uint32_t gCANapiMem[MAX_CAN_PARAM_SIZE];

#define BUFF_IDLE					0		// This represents an available buffer
#define BUFF_STATIC					1		// This represents a statically assigned buffer
#define BUFF_TEMP					2		// This represents a temporarily assigned buffer

// Bit Timings for Different CAN speeds
const uint32_t bitTimingTable[] = {
	0x00001CE0UL,	/* 10Khz bit rate  (%87.5 w 36Mhz base clock ) */
	0x00001BF7UL,	/* 20Khz bit rate  (%86.7 w 36Mhz base clock ) */
	0x00001CECUL,	/* 50Khz bit rate  (%87.5 w 36Mhz base clock ) */
	0x00001BD7UL,	/* 100Khz bit rate  (%86.7 w 36Mhz base clock ) */
	0x00001CD1UL,	/* 125Khz bit rate  (%87.5 w 36Mhz base clock ) */
	0x00001CD1UL,	/* 250Khz bit rate  (%87.5 w 72Mhz base clock ) */
	0x00001CC8UL,	/* 500Khz bit rate  (%87.5 w 72Mhz base clock ) */
	0x00001BC5UL,	/* 800Khz bit rate  (%86.7 w 72Mhz base clock ) */
	0x000009C5UL,	/* 1MHz bit rate  (%91.7 w 72Mhz base clock ) */
};

const uint32_t baudClkDivTable[] = {
	0x00000001UL,	/* 10Khz bit rate  (36 MHz Core Clock, Div = 2) */
	0x00000001UL,	/* 20Khz bit rate  (36 MHz Core Clock, Div = 2) */
	0x00000001UL,	/* 50Khz bit rate  (36 MHz Core Clock, Div = 2) */
	0x00000001UL,	/* 100Khz bit rate  (36 MHz Core Clock, Div = 2) */
	0x00000001UL,	/* 125Khz bit rate  (36 MHz Core Clock, Div = 2) */
	0x00000000UL,	/* 250Khz bit rate  (72 MHz Core Clock, Div = 1) */
	0x00000000UL,	/* 500Khz bit rate  (72 MHz Core Clock, Div = 1) */
	0x00000000UL,	/* 800Khz bit rate  (72 MHz Core Clock, Div = 1) */
	0x00000000UL,	/* 1MHz bit rate  (72 MHz Core Clock, Div = 1) */
};

uint8_t buff_stat[CP_BUFFER_MAX];

// Global Counters
_U32 globalTxCount = 0;
_U32 globalRxCount = 0;
_U32 globalErrCount = 0;

// CAN ROM Structs
CAN_CFG gCanConfig;
CAN_HANDLE_T pCanHandle;

/* Helper Functions */
extern _U08 buffer_busy(_U08 ubBufferIdxV);

/* Callback function prototypes */
static void CAN_rx(uint8_t msg_obj_num);
static void CAN_tx(uint8_t msg_obj_num);
static void CAN_error(uint32_t error_info);

/* Publish CAN Callback Functions */
CAN_CALLBACKS callbacks = {
	CAN_rx,
	CAN_tx,
	CAN_error
};

/* Need to think about how I implement the callback functions */
CAN_API_INIT_PARAM_T CANConfig = {
		0,
		LPC_C_CAN0_BASE,
		&gCanConfig,
		&callbacks,
		NULL,
		NULL,
};

//-------------------------------------------------------------------
// these pointers store the callback handlers, and whether they are initialized
// (can I just check the pointer to see if it's NULL?)
_U08			RcvIntHandlerInit = 0;
_U08			TrmIntHandlerInit = 0;
_U08			ErrIntHandlerInit = 0;
_U08           (* pfnRcvIntHandler) (_TsCpCanMsg *, _U08);
_U08           (* pfnTrmIntHandler) (_TsCpCanMsg *, _U08);
_U08           (* pfnErrIntHandler) (_U08);


/* Is this necessary? */
void CAN_IRQHandler(void)
{
	LPC_CAND_API->hwCAN_Isr(pCanHandle);
}

/* CAN Receive Callback Function */
static void CAN_rx(_U08 buffer_id) {
	globalRxCount++;
	// Instantiate a message object structure with relevant buffer id to receive the CAN message
	CAN_MSG_OBJ rcv_obj;
	rcv_obj.msgobj = buffer_id;

	// Now load the message from mailbox ROM to msgobj structure
	LPC_CAND_API->hwCAN_MsgReceive(pCanHandle, (CAN_MSG_OBJ*)&rcv_obj);

	// Convert msg object structure to a CANpie msg structure
	_TsCpCanMsg cp_rcv_msg;

	//Helper function, does this have to be inline?
	msgobj_to_cpmsg(&rcv_obj, &cp_rcv_msg);

	// We now have full conversion into a CpCanMsg so we call the installed callbacks if any
	if (RcvIntHandlerInit) {
		pfnRcvIntHandler(&cp_rcv_msg, buffer_id);
	}
}

/* CAN Transmit Callback Function */
static void CAN_tx(uint8_t buffer_id) {
	globalTxCount++;

	// If this was a temporary transmission request release the buffer
	if (buff_stat[buffer_id] == BUFF_TEMP) {
		buff_stat[buffer_id] = BUFF_IDLE;
	}

	if (TrmIntHandlerInit) {
		pfnTrmIntHandler(NULL, buffer_id);
	}
}

/* CAN Error Callback Function */
static void CAN_error(uint32_t error_info) {
	globalErrCount++;

	if (ErrIntHandlerInit) {
		pfnErrIntHandler(error_info & 0xFF);
	}
}

/*!
** \brief   Run automatic bitrate detection
** \param   ptsPortV       Pointer to CAN port structure
**
** \return  LPC15xx uC's do not support auto baudrate detection.
** 			NOT_SUPPORTED error returned.
**
**
*/
_TvCpStatus CpCoreAutobaud(_TsCpPort * ptsPortV, _U08 * pubBaudSelV, _U16 * puwWaitV) {
	return CpErr_NOT_SUPPORTED;
}

/*!
** \brief   Set bitrate of CAN controller
** \param   ptsPortV       Pointer to CAN port structure
** \param   ubBaudSelV     Bitrate selection
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function directly writes to the bit timing registers of the CAN
** controller. The value for the parameter \e ubBaudSelV is taken
** from the CP_BAUD enumeration.
**
*/
_TvCpStatus CpCoreBaudrate(_TsCpPort * ptsPortV, _U08 ubBaudSelV){

	gCanConfig.clkdiv = baudClkDivTable[ubBaudSelV];
	gCanConfig.btr = bitTimingTable[ubBaudSelV];

//	// DEBUG DEBUG DEBUG
//	gCanConfig.clkdiv = 0x00000001UL;
//	gCanConfig.btr = 0x00001BD7UL;

//	/* Initialize with new bitrate, is this necessary?*/
//	status = LPC_CAND_API->hwCAN_Init(&pCanHandle, &CANConfig);
//	if (status != CAN_ERROR_NONE) {
//		return CpErr_HARDWARE;
//	}

	return CpErr_OK;
}

/*!
** \brief   Set bitrate of CAN controller
** \param   ptsPortV       Pointer to CAN port structure
** \param   ptsBitrateV    Pointer to bit timing structure
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function directly writes to the bit timing registers of the CAN
** controller. Usage of the function requires a detailed knowledge of
** the used CAN controller hardware.
**
*/
_TvCpStatus CpCoreBittiming(_TsCpPort * ptsPortV, _TsCpBitTiming * ptsBitrateV){
	return CpErr_NOT_SUPPORTED;
}

/*!
** \brief   Set state of CAN controller
** \param   ptsPortV       Pointer to CAN port structure
** \param   ubModeV        Mode selection
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function changes the operating mode of the CAN controller.
** Possible values for mode are defined in the #CP_MODE enumeration.
*/
_TvCpStatus CpCoreCanMode(_TsCpPort * ptsPortV, _U08 ubModeV) {
	switch (ubModeV) {
	case CP_MODE_STOP:
		if (gCanConfig.isr_ena == TRUE) {
			NVIC_DisableIRQ(CAN_IRQn);
		}
		else {
			return CpErr_HARDWARE;
		}
		break;

	case CP_MODE_START:
		gCanConfig.isr_ena = TRUE;

		if (LPC_CAND_API->hwCAN_Init(&pCanHandle, &CANConfig) != CAN_ERROR_NONE) {
			return CpErr_HARDWARE;
		}

		#ifdef LOOPBACK_MODE					//LOOPBACK
		 LPC_CAN->CNTL |= (CTRL_TEST);			//enable test
		 LPC_CAN->TEST |= (TEST_LBACK);			//enable loopback
		#endif
		if (gCanConfig.isr_ena == TRUE) {
			NVIC_EnableIRQ(CAN_IRQn);
		}
		else {
			return CpErr_HARDWARE;
		}
		break;

	case CP_MODE_LISTEN_ONLY:
		//Not implemented yet
		return CpErr_NOT_SUPPORTED;
		break;

	case CP_MODE_SLEEP:
		//Not Implemented yet
		return CpErr_NOT_SUPPORTED;
		break;

	default:
		return CpErr_NOT_SUPPORTED;
	}

	return CpErr_OK;
}

/*!
** \brief   Retrieve status of CAN controller
** \param   ptsPortV       Pointer to CAN port structure
** \param   ptsStateV      Pointer to CAN state structure
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function retrieved the present state of the CAN controller. Possible
** values are defined in the #CP_STATE enumeration. The state of the CAN
** controller is copied to the variable pointer 'ptsStateV'.
*/
_TvCpStatus CpCoreCanState(_TsCpPort * ptsPortV, _TsCpState * ptsStateV) {
	uint32_t status = LPC_CAN->STAT;

	/* There seems to be a discrepancy between the defined error masks in
	 * rom_can_15xx.h and the CAN status register in the User Guide. They
	 * might not be meant to be the same so we use the user guide register
	 * as our source.
	 */

	uint32_t lec = status & STAT_LEC;
	switch (lec) {
	case 0x1:
		ptsStateV->ubCanErrType = CP_ERR_TYPE_STUFF;
		break;

	case 0x2:
		ptsStateV->ubCanErrType = CP_ERR_TYPE_FORM;
		break;

	case 0x3:
		ptsStateV->ubCanErrType = CP_ERR_TYPE_ACK;
		break;

	case 0x4:
		ptsStateV->ubCanErrType = CP_ERR_TYPE_BIT1;
		break;

	case 0x5:
		ptsStateV->ubCanErrType = CP_ERR_TYPE_BIT0;
		break;

	case 0x6:
		ptsStateV->ubCanErrType = CP_ERR_TYPE_CRC;
		break;

	default:
		ptsStateV->ubCanErrType = CP_ERR_TYPE_NONE;
		ptsStateV->ubCanErrState = CP_STATE_ACTIVE;
		return (CpErr_OK);
		break;
	}


	if (status & STAT_BOFF) {
		ptsStateV->ubCanErrState |= CP_STATE_BUS_OFF;
	}

	if (status & STAT_EWARN) {
		ptsStateV->ubCanErrState |= CP_STATE_BUS_WARN;
	}

	if (status & STAT_EPASS) {
		ptsStateV->ubCanErrState |= CP_STATE_BUS_PASSIVE;
	}

	uint32_t error_counter = LPC_CAN->EC;
	ptsStateV->ubCanTrmErrCnt = error_counter & EC_TXEC;
	ptsStateV->ubCanTrmErrCnt = error_counter & EC_RXEC;

	return (CpErr_OK);
}

/*!
** \brief   Initialise the CAN driver
** \param   ubPhyIfV     CAN channel of the hardware
** \param   ptsPortV     Pointer to CAN port structure
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** \see     CpCoreDriverRelease()
**
** The functions opens the physical CAN interface defined by the
** parameter \c ubPhyIfV. The value for \c ubPhyIfV is taken from
** the enumeration CP_CHANNEL. The function sets up the field
** members of the CAN port handle \c ptsPortV. On success, the
** function returns CpErr_OK. On failure, the function can return
** the following values:
** <ul>
** <li>#CpErr_HARDWARE
** <li>#CpErr_INIT_FAIL
** </ul>
** An opened handle to a CAN port must be closed via the CpCoreDriverRelease()
** function.
*/
_TvCpStatus CpCoreDriverInit(_U08 ubPhyIfV, _TsCpPort * ptsPortV){

	uint32_t maxParamSize;
//	uint32_t status;
	//CAND_API_T* pCANApi;

	/* Enable clocking for CAN and reset the controller */
	Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_CAN);
	Chip_SYSCTL_PeriphReset(RESET_CAN);

	/* Assign the pins rx ??? and tx ??? */
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 11, (IOCON_MODE_INACT | IOCON_DIGMODE_EN));
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 31, (IOCON_MODE_INACT | IOCON_DIGMODE_EN));
	Chip_SWM_MovablePinAssign(SWM_CAN_RD1_I, 11);	/* Rx P0.11 */
	Chip_SWM_MovablePinAssign(SWM_CAN_TD1_O, 31);	/* Tx P0.31 */

	// Not really sure, need to look into what this means exactly
	CANConfig.mem_base = (uint32_t) &gCANapiMem[0];

	/* Set the Can peripheral to run at 12Mhz. Was used to calculate the btr value in next line. */
	/* CPU runs from Xtal at 72Mhz so the divisor should be 6 but clkdiv is divisor - 1 */
	gCanConfig.clkdiv = 5;

	/* btr values can be obtained from www.bittiming.can-wiki.info  (use Bosch C_CAN / D_CAN) */
	gCanConfig.btr = 0x1c0e;	/* 50Khz bit rate  (%87.5 w 12Mhz base clock ) */

	// DEBUG DEBUG DEBUG
	gCanConfig.clkdiv = 0x00000001UL;
	gCanConfig.btr = 0x00001BD7UL;

//	gCanConfig.isr_ena = TRUE;

	/* Validate that we reserved enough memory */
	maxParamSize = LPC_CAND_API->hwCAN_GetMemSize(&CANConfig);
	if (maxParamSize > MAX_CAN_PARAM_SIZE / 4) { while ( 1 ) {} }

//	/* Do we actually need this here? Should test without */
//	status = LPC_CAND_API->hwCAN_Init(&pCanHandle, &CANConfig);
//	// I doubt this works we need to setup a timeout
//	if (status != CAN_ERROR_NONE) {
//		return CpErr_HARDWARE;
//	}
//
//	#ifdef LOOPBACK_MODE					//LOOPBACK
//	 LPC_CAN->CNTL |= (CTRL_TEST);			//enable test
//	 LPC_CAN->TEST |= (TEST_LBACK);			//enable loopback
//	#endif

	return CpErr_OK;

}

/*!
** \brief   Release the CAN driver
** \param   ptsPortV       Pointer to CAN port structure
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** \see     CpCoreDriverInit()
**
** The implementation of this function is dependent on the operating
** system. Typical tasks might be:
** <ul>
** <li>clear the interrupt vector / routine
** <li>close all open paths to the hardware
** </ul>
**
*/
_TvCpStatus CpCoreDriverRelease(_TsCpPort * ptsPortV) {
	CpCoreCanMode(ptsPortV, CP_MODE_STOP);
	return (CpErr_OK);
}

_TvCpStatus CpCoreHDI(_TsCpPort * ptsPortV, _TsCpHdi * ptsHdiV) {
	ptsHdiV->uwVersionNumber = 1;
	ptsHdiV->uwSupportFlags = 0x001E;
	ptsHdiV->uwControllerType = CP_CC_LPC1549;
	ptsHdiV->uwIRQNumber = 0;
	ptsHdiV->uwBufferMax = CP_BUFFER_MAX;

	ptsHdiV->ulBitrate = gCanConfig.btr;
	ptsHdiV->ulTimeStampRes = 0;
	ptsHdiV->ulCanClock = 0;
	ptsHdiV->uwRes = 0;

	return (CpErr_OK);
}

_TvCpStatus CpCoreIntFunctions(_TsCpPort * ptsPortV,
								_U08 (* pfnRcvHandler) (_TsCpCanMsg *, _U08),
								_U08 (* pfnTrmHandler) (_TsCpCanMsg *, _U08),
								_U08 (* pfnErrHandler) (_U08)                ) {

	if (pfnRcvHandler != NULL) {
		pfnRcvIntHandler = pfnRcvHandler;
		RcvIntHandlerInit = 1;
	}

	if (pfnTrmHandler != NULL) {
		pfnTrmIntHandler = pfnTrmHandler;
		TrmIntHandlerInit = 1;
	}

	if (pfnErrHandler != NULL) {
		pfnErrIntHandler = pfnErrHandler;
		ErrIntHandlerInit = 1;
	}

	return (CpErr_OK);
}

_TvCpStatus CpCoreMsgRead( _TsCpPort * ptsPortV, _TsCpCanMsg * ptsBufferV, _U32 * pulBufferSizeV)
{
	return CpErr_NOT_SUPPORTED;
}

_TvCpStatus CpCoreMsgWrite( _TsCpPort * ptsPortV, _TsCpCanMsg * ptsBufferV, _U32 * pulBufferSizeV)
{
	CAN_MSG_OBJ tx_obj;

	while (*pulBufferSizeV > 0) {
		cpmsg_to_msgobj(ptsBufferV, &tx_obj);

		// Find an empty message buffer to use
		_U08 bufIndex = 0;

		while (bufIndex < CP_BUFFER_MAX) {
			if (buff_stat[bufIndex] == BUFF_IDLE) {
				break;
			}
			if (bufIndex == CP_BUFFER_MAX - 1) {
				return CpErr_TRM_FULL;
			}
			bufIndex++;
		}

		tx_obj.msgobj = bufIndex;
		buff_stat[bufIndex] = BUFF_TEMP;

		// Stall if previous tx of this mailbox hasn't completed
		while(buffer_busy(bufIndex+1))
			;

		LPC_CAND_API->hwCAN_MsgTransmit(pCanHandle, &tx_obj);

//		// Fuck the ROM it's so shit
//		if (CpMsgIsExtended(ptsBufferV)) {
//			// Extended frame
//			_U32 id = CpMsgGetExtId(ptsBufferV);
//
//			// Load arbitration registers with the ID
//			LPC_CAN->IF1_ARB1 = id & 0x0000FFFF;
//			LPC_CAN->IF1_ARB2 = (id >> 16) & 0x1FFF;
//
//			// Specify extended identifier
//			LPC_CAN->IF1_ARB2 |= ID_MTD;
//
//			// This is saying not to mask any of the bits in the ext ID
//			LPC_CAN->IF1_MSK2 = MASK_MXTD | MASK_MDIR | (ID_EXT_MASK >> 16);
//			LPC_CAN->IF1_MSK1 = ID_EXT_MASK & 0x0000FFFF;
//		}
//		else {
//			// Standard frame
//			_U32 id = CpMsgGetStdId(ptsBufferV);
//
//			// Load arbitration registers with the ID
//			LPC_CAN->IF1_ARB2 = (id << 2) & 0x00001FFF;
//
//			// Ensure standard ID specified
//			LPC_CAN->IF1_ARB2 &= ~ID_MTD;
//
//			// Just saying don't mask any bits of the STD ID
//			LPC_CAN->IF1_MSK2 = MASK_MDIR | (ID_STD_MASK << 2);
//			LPC_CAN->IF1_MSK1 = 0x0000;
//		}
//
//		/* Put all the data into data registers */
//		LPC_CAN->IF1_DA1 = ptsBufferV->tuMsgData.auwWord[0];
//		LPC_CAN->IF1_DA2 = ptsBufferV->tuMsgData.auwWord[1];
//		LPC_CAN->IF1_DB1 = ptsBufferV->tuMsgData.auwWord[2];
//		LPC_CAN->IF1_DB2 = ptsBufferV->tuMsgData.auwWord[3];
//
//		/* Specify buffer direction now and flag that the message object is configured */
//		LPC_CAN->IF1_ARB2 |= ID_DIR | ID_MVAL;
//
//		/* Set the length DLC field, set transmission request and specify no FIFO buffer */
//		LPC_CAN->IF1_MCTRL = UMSK | RXIE | TXRQ | EOB | (ptsBufferV->ubMsgDLC & DLC_MASK);
//
//		/* Specify what data is written to the mailbox */
//		LPC_CAN->IF1_CMDMSK = WR | MASK | ARB | CTRL | DATAA | DATAB;
//
//		/* Transfer it to the desired mailbox */
//		LPC_CAN->IF1_CMDREQ = bufIndex+1;
//
//		/* Stall until transfer is complete */
//		while (LPC_CAN->IF1_CMDREQ & IFCREQ_BUSY);

		(*pulBufferSizeV)--;
		ptsBufferV++;
	}

	return CpErr_OK;
}

_TvCpStatus CpCoreStatistic(_TsCpPort * ptsPortV, _TsCpStatistic * ptsStatsV)
{
	return CpErr_NOT_SUPPORTED;
}

/*!
** \brief   Initialise buffer in FullCAN controller
** \param   ptsPortV       Pointer to CAN port structure
** \param   ptsCanMsgV     Pointer to a CAN message structure
** \param   ubBufferIdxV   Buffer number
** \param   ubDirectionV   Direction of message
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** \see     CpCoreBufferRelease()
**
** This function allocates the message buffer in a FullCAN controller.
** The number of the message buffer inside the FullCAN controller is
** denoted via the parameter \c ubBufferIdxV.
** The parameter \c ubDirectionV can have the following values:
** \li CP_BUFFER_DIR_RX: receive
** \li CP_BUFFER_DIR_TX: transmit
**
** The following example shows the setup of a transmit buffer
** \dontinclude buf_init.c
** \skip    void AllocateTrmBuffer
** \until   }
**
** An allocated transmit buffer can be sent via the function
** CpCoreBufferSend().
*/
_TvCpStatus CpCoreBufferInit( _TsCpPort * ptsPortV, _TsCpCanMsg * ptsCanMsgV, _U08 ubBufferIdxV, _U08 ubDirectionV) {
	ubBufferIdxV += 1;

	/* wait until IF1 is free */
	while( LPC_CAN->IF1_CMDREQ & IFCREQ_BUSY )
		;

	if (ubDirectionV == CP_BUFFER_DIR_TX) {
		// Configure a transmit mailbox
		if (CpMsgIsExtended(ptsCanMsgV)) {
			_U32 id = CpMsgGetExtId(ptsCanMsgV);

			// Load arbitration registers with the ID
			LPC_CAN->IF1_ARB1 = id & 0x0000FFFF;
			LPC_CAN->IF1_ARB2 = (id >> 16) & 0x1FFF;

			// Specify extended identifier
			LPC_CAN->IF1_ARB2 |= ID_MTD;

			// This is saying not to mask any of the bits in the ext ID
			LPC_CAN->IF1_MSK2 = MASK_MXTD | MASK_MDIR | (ID_EXT_MASK >> 16);
			LPC_CAN->IF1_MSK1 = ID_EXT_MASK & 0x0000FFFF;

		}
		else {
			_U32 id = (_U32) CpMsgGetStdId(ptsCanMsgV);

			// Load arbitration registers with the ID
			LPC_CAN->IF1_ARB2 = (id << 2) & 0x00001FFF;

			// Ensure standard ID specified
			LPC_CAN->IF1_ARB2 &= ~ID_MTD;

			// Just saying don't mask any bits of the STD ID
			LPC_CAN->IF1_MSK2 = MASK_MDIR | (ID_STD_MASK << 2);
			LPC_CAN->IF1_MSK1 = 0x0000;
		}

		// Load the data now
		LPC_CAN->IF1_DA1 = ptsCanMsgV->tuMsgData.auwWord[0];
		LPC_CAN->IF1_DA2 = ptsCanMsgV->tuMsgData.auwWord[1];
		LPC_CAN->IF1_DB1 = ptsCanMsgV->tuMsgData.auwWord[2];
		LPC_CAN->IF1_DB2 = ptsCanMsgV->tuMsgData.auwWord[3];

		// Specify buffer direction now and flag that the message object is configured
		LPC_CAN->IF1_ARB2 |= ID_DIR | ID_MVAL;

		/* set the length DLC field and specify no FIFO buffer */
		LPC_CAN->IF1_MCTRL = UMSK | EOB | (ptsCanMsgV->ubMsgDLC & DLC_MASK);

		/* Specify what data is written to the mailbox */
		LPC_CAN->IF1_CMDMSK = WR | MASK | ARB | CTRL | DATAA | DATAB;

		/* Finally, write all this data to the specified mailbox */
		LPC_CAN->IF1_CMDREQ = ubBufferIdxV;

		while ( LPC_CAN->IF1_CMDREQ & IFCREQ_BUSY )
			; /* Waits until IF1 transfer to mailbox is complete*/

	}
	else {
		// Configure a receive mailbox

		/* Note for this we'll load the acceptance mask into the lower byte of the
		 * data field of the CpMsg
		 */

		_U32 mask = ptsCanMsgV->tuMsgData.aulLong[0];

		if (CpMsgIsExtended(ptsCanMsgV)) {
			_U32 id = CpMsgGetExtId(ptsCanMsgV);

			/* set the filter mask registers */
			LPC_CAN->IF1_MSK1 = mask & 0xFFFF;
			LPC_CAN->IF1_MSK2 = MASK_MXTD | (mask >> 16);

			// Load arbitration registers with the ID
			LPC_CAN->IF1_ARB1 = id & 0x0000FFFF;
			LPC_CAN->IF1_ARB2 = (id >> 16) & 0x1FFF;

			// Specify extended identifier
			LPC_CAN->IF1_ARB2 |= ID_MTD;
		}
		else {
			_U32 id = (_U32) CpMsgGetStdId(ptsCanMsgV);

			// Load arbitration registers with the ID
			LPC_CAN->IF1_ARB2 = (id << 2) & 0x00001FFF;

			// Ensure standard ID specified
			LPC_CAN->IF1_ARB2 &= ~ID_MTD;

			// Set filter mask
			LPC_CAN->IF1_MSK2 = (mask << 2);
		}

		// Is this necessary?
		LPC_CAN->IF1_DA1 = 0x0000;
		LPC_CAN->IF1_DA2 = 0x0000;
		LPC_CAN->IF1_DB1 = 0x0000;
		LPC_CAN->IF1_DB2 = 0x0000;

		LPC_CAN->IF1_ARB2 &= ~ID_DIR; // receive direction
		LPC_CAN->IF1_ARB2 |= ID_MVAL; // contents are valid

		// Enable RX interrupts and set DLC mask
		LPC_CAN->IF1_MCTRL = UMSK | RXIE | EOB | (ptsCanMsgV->ubMsgDLC & DLC_MASK);

		/* Specify what data is written to the mailbox */
		LPC_CAN->IF1_CMDMSK = WR | MASK | ARB | CTRL | DATAA | DATAB;

		/* Transfer data to message RAM */
		LPC_CAN->IF1_CMDREQ = ubBufferIdxV;

		/* wait until it's done */
		while( LPC_CAN->IF1_CMDREQ & IFCREQ_BUSY )
			;

	}

	// Flag that this buffer has been initialized
	buff_stat[ubBufferIdxV-1] = BUFF_STATIC;

	return (CpErr_OK);

}

/*!
** \brief   Get data from message buffer
** \param   ptsPortV       Pointer to CAN port structure
** \param   ubBufferIdxV   Buffer number
** \param   pubDataV       Buffer for data
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function is the fastest method to get data from a FullCAN message
** buffer. The buffer has to be configured by a call to CpCoreBufferInit() before.
**
*/
_TvCpStatus CpCoreBufferGetData(_TsCpPort * ptsPortV, _U08 ubBufferIdxV, _U08 * pubDataV)
{
	if (buff_stat[ubBufferIdxV-1] == BUFF_IDLE) {
		return CpErr_INIT_MISSING;
	}

	// We use IF2 for all receive methods, make sure it's not busy
	while ( LPC_CAN->IF2_CMDREQ & IFCREQ_BUSY )
			;

	// Basically transfer all data from the wanted message object into IF2 registers
	// We could get away with just DATAA and DATAB of course
	LPC_CAN->IF2_CMDMSK = RD | DATAA | DATAB;
	LPC_CAN->IF2_CMDREQ = ubBufferIdxV;    /* Start message transfer */

	// Wait for request to complete
	while ( LPC_CAN->IF2_CMDREQ & IFCREQ_BUSY )
		;

	*pubDataV++ = LPC_CAN->IF2_DA1 & 0xFF;
	*pubDataV++ = LPC_CAN->IF2_DA1 & 0xFF00;
	*pubDataV++ = LPC_CAN->IF2_DA2 & 0xFF;
	*pubDataV++ = LPC_CAN->IF2_DA2 & 0xFF00;
	*pubDataV++ = LPC_CAN->IF2_DB1 & 0xFF;
	*pubDataV++ = LPC_CAN->IF2_DB1 & 0xFF00;
	*pubDataV++ = LPC_CAN->IF2_DB1 & 0xFF;
	*pubDataV++ = LPC_CAN->IF2_DB1 & 0xFF00;

	return (CpErr_OK);
}

/*!
** \brief   Get DLC of specified buffer
** \param   ptsPortV       Pointer to CAN port structure
** \param   ubBufferIdxV   Buffer number
** \param   pubDlcV        Data Length Code
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function retrieves the Data Length Code (DLC) of the selected buffer
** \c ubBufferIdxV.
*/
_TvCpStatus CpCoreBufferGetDlc( _TsCpPort * ptsPortV, _U08 ubBufferIdxV, _U08 * pubDlcV)
{
	if (buff_stat[ubBufferIdxV-1] == BUFF_IDLE) {
		return CpErr_INIT_MISSING;
	}

	// We use IF2 for all receive methods, make sure it's not busy
	while ( LPC_CAN->IF2_CMDREQ & IFCREQ_BUSY )
			;

	// Basically transfer all data from the wanted message object into IF2 registers
	// We could get away with just DATAA and DATAB of course
	LPC_CAN->IF2_CMDMSK = RD | CTRL;
	LPC_CAN->IF2_CMDREQ = ubBufferIdxV;    /* Start message transfer */

	// Wait for request to complete
	while ( LPC_CAN->IF2_CMDREQ & IFCREQ_BUSY )
		;

	*pubDlcV = LPC_CAN->IF2_MCTRL & DLC_MASK;

	return CpErr_OK;
}

/*!
** \brief   Release message buffer of FullCAN controller
** \param   ptsPortV       Pointer to CAN port structure
** \param   ubBufferIdxV   Buffer number
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** \see     CpCoreBufferInit()
*/
_TvCpStatus CpCoreBufferRelease( _TsCpPort * ptsPortV, _U08 ubBufferIdxV)
{
	if (buff_stat[ubBufferIdxV-1] == BUFF_IDLE) {
		return CpErr_INIT_MISSING;
	}

	// Mark the buffer as free for overwrite
	buff_stat[ubBufferIdxV-1] = BUFF_IDLE;

	return CpErr_OK;
}

/*!
** \brief   Send message from message buffer
** \param   ptsPortV       Pointer to CAN port structure
** \param   ubBufferIdxV   Index of message buffer
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function transmits a message from the specified message buffer
** \c ubBufferIdxV. The message buffer has to be configured by a call to
** CpCoreBufferInit() in advance. The first message buffer starts at
** the index CP_BUFFER_1.
**
*/
_TvCpStatus CpCoreBufferSend(_TsCpPort * ptsPortV, _U08 ubBufferIdxV)
{
	if (buff_stat[ubBufferIdxV-1] == BUFF_IDLE) {
		return CpErr_INIT_MISSING;
	}

	// Check if the buffer is currently queued for transmission
	if (buffer_busy(ubBufferIdxV)) {
		return CpErr_BUFF_BUSY;
	}

	// We use IF1 for all transmit methods, make sure it's not busy
	while ( LPC_CAN->IF1_CMDREQ & IFCREQ_BUSY )
		;

	// Basically request a transmission for this mailbox (should already be configured with right data)
	LPC_CAN->IF1_CMDMSK = WR | TREQ;
	LPC_CAN->IF1_CMDREQ = ubBufferIdxV;    /* Start message transfer */

	// Wait for request to complete
	while ( LPC_CAN->IF1_CMDREQ & IFCREQ_BUSY )
		;

	return CpErr_OK;
}

/*!
** \brief   Set data of message buffer
** \param   ptsPortV       Pointer to CAN port structure
** \param   ubBufferIdxV   Buffer number
** \param   pubDataV       Pointer to data buffer
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function is the fastest method to set the data bytes of a CAN message.
** It can be used in combination with the function CpCoreBufferSend(). It
** will write 8 data bytes into the buffer defined by \e ubBufferIdxV. The
** buffer has to be configured by CpCoreBufferInit() in advance. The size
** of the data buffer \e pubDataV must have a size of 8 bytes.
**
** The following example demonstrates the access to the data bytes of a CAN
** message:
** \code
**  _U08 aubDataT[8];   // buffer for 8 bytes
**
** aubDataT[0] = 0x11;  // byte 0: set to 11hex
** aubDataT[1] = 0x22;  // byte 1: set to 22hex

** //--- copy the stuff to message buffer 1 ---------------
** CpCoreBufferSetData(CP_CHANNEL_1, CP_BUFFER_1, &aubDataT);
**
** //--- send this message out ----------------------------
** CpCoreBufferSend(CP_CHANNEL_1, CP_BUFFER_1);
** \endcode
**
*/
_TvCpStatus CpCoreBufferSetData( _TsCpPort * ptsPortV, _U08 ubBufferIdxV, _U08 * pubDataV)
{
	if (buff_stat[ubBufferIdxV-1] == BUFF_IDLE) {
		return CpErr_INIT_MISSING;
	}

	// Check if the buffer is currently queued for transmission
	if (buffer_busy(ubBufferIdxV)) {
		return CpErr_BUFF_BUSY;
	}

	// We use IF1 for all transmit methods, make sure it's not busy
	while ( LPC_CAN->IF1_CMDREQ & IFCREQ_BUSY )
		;

	// Set DATA A and DATA B registers
	LPC_CAN->IF1_DA1 = *(pubDataV++);
	LPC_CAN->IF1_DA1 |= *(pubDataV++) << 8;
	LPC_CAN->IF1_DA2 = *(pubDataV++);
	LPC_CAN->IF1_DA2 |= *(pubDataV++) << 8;
	LPC_CAN->IF1_DB1 = *(pubDataV++);
	LPC_CAN->IF1_DB1 |= *(pubDataV++) << 8;
	LPC_CAN->IF1_DB2 = *(pubDataV++);
	LPC_CAN->IF1_DB2 |= *(pubDataV++) << 8;

	// Request new data shifted into mailbox
	LPC_CAN->IF1_CMDMSK = WR | DATAA | DATAB;
	LPC_CAN->IF1_CMDREQ = ubBufferIdxV;

	// Wait for request to complete
	while ( LPC_CAN->IF1_CMDREQ & IFCREQ_BUSY )
		;

	return CpErr_OK;
}

/*!
** \brief   Set DLC of specified buffer
** \param   ptsPortV       Pointer to CAN port structure
** \param   ubBufferIdxV   Buffer number
** \param   ubDlcV         Data Length Code
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function sets the Data Length Code (DLC) of the selected buffer
** ubBufferIdxV. The DLC must be in the range from 0 to 8.
*/
_TvCpStatus CpCoreBufferSetDlc( _TsCpPort * ptsPortV, _U08 ubBufferIdxV, _U08 ubDlcV)
{
	if (buff_stat[ubBufferIdxV-1] == BUFF_IDLE) {
		return CpErr_INIT_MISSING;
	}

	// We use IF1 for all transmit methods, make sure it's not busy
	while ( LPC_CAN->IF1_CMDREQ & IFCREQ_BUSY )
		;

	// Prepare message control bits with appropriate DLC
	LPC_CAN->IF1_MCTRL = EOB | TXIE | UMSK | (ubDlcV & DLC_MASK);

	// Write control bits to mailbox
	LPC_CAN->IF1_CMDMSK = WR | CTRL;
	LPC_CAN->IF1_CMDREQ = ubBufferIdxV;

	// Wait for request to complete
	while ( LPC_CAN->IF1_CMDREQ & IFCREQ_BUSY )
		;

	return CpErr_OK;
}

