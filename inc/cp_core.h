//****************************************************************************//
// File:          cp_core.h                                                   //
// Description:   CANpie core functions                                       //
// Author:        Uwe Koppe                                                   //
// e-mail:        koppe@microcontrol.net                                      //
//                                                                            //
// Copyright (C) MicroControl GmbH & Co. KG                                   //
// 53842 Troisdorf - Germany                                                  //
// www.microcontrol.net                                                       //
//                                                                            //
//----------------------------------------------------------------------------//
// Redistribution and use in source and binary forms, with or without         //
// modification, are permitted provided that the following conditions         //
// are met:                                                                   //
// 1. Redistributions of source code must retain the above copyright          //
//    notice, this list of conditions, the following disclaimer and           //
//    the referenced file 'COPYING'.                                          //
// 2. Redistributions in binary form must reproduce the above copyright       //
//    notice, this list of conditions and the following disclaimer in the     //
//    documentation and/or other materials provided with the distribution.    //
// 3. Neither the name of MicroControl nor the names of its contributors      //
//    may be used to endorse or promote products derived from this software   //
//    without specific prior written permission.                              //
//                                                                            //
// Provided that this notice is retained in full, this software may be        //
// distributed under the terms of the GNU Lesser General Public License       //
// ("LGPL") version 3 as distributed in the 'COPYING' file.                   //
//                                                                            //
//----------------------------------------------------------------------------//
//                                                                            //
// Date        History                                                        //
// ----------  -------------------------------------------------------------- //
// 29.07.2003  Initial version                                                //
//                                                                            //
//****************************************************************************//


#ifndef  _CP_CORE_H_
#define  _CP_CORE_H_


//-----------------------------------------------------------------------------
// SVN  $Date: 2008-11-11 21:12:53 +0100 (Di, 11 Nov 2008) $
// SVN  $Rev: 149 $ --- $Author: microcontrol $
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
/*!
** \file    cp_core.h
** \brief   CANpie core functions
**
** The core functions provide the direct interface to the CAN controller
** (hardware). Please note that due to hardware limitations maybe certain
** functions are not implemented on the target platform. A call to an
** unsupported function will always return the error code
** #CpErr_NOT_SUPPORTED.
** <p>
** For a "FullCAN" controller (i.e. a CAN controller that has several message
** buffers) an extended set of powerful functions (CpCoreBuffer..())is provided.
** <p>
** <h3>Initialisation Process</h3>
** <p>
** The CAN driver is initialised with the function CpCoreDriverInit(). This
** routine will setup the CAN controller, but not configure a certain bitrate
** nor switch the CAN controller to active operation. The following core
** functions must be called in that order:
** \li CpCoreDriverInit()
** \li CpCoreBaudrate() / CpCoreBittiming()
** \li CpCoreCanMode()
**
** The function CpCoreDriverInit() must be called before any other core
** function in order to have a valid handle / pointer to the open CAN interface.
**
** \b Example
** \code
** void MyCanInit(void)
** {
**    _TsCpPort tsCanPortT; // logical CAN port
**    //---------------------------------------------------
**    // setup the CAN controller / open a physical CAN
**    // port
**    //
**    CpCoreDriverInit(CP_CHANNEL_1, &tsCanPortT);
**    //---------------------------------------------------
**    // setup 500 kBit/s
**    //
**    CpCoreBaudrate(&tsCanPortT, CP_BAUD_500K);
**    //---------------------------------------------------
**    // start CAN operation
**    //
**    CpCoreCanMode(&tsCanPortT, CP_MODE_START);
**    //.. now we are operational
** }
** \endcode
*/


/*----------------------------------------------------------------------------*\
** Include files                                                              **
**                                                                            **
\*----------------------------------------------------------------------------*/

#include "canpie.h"



/*----------------------------------------------------------------------------*\
** Definitions                                                                **
**                                                                            **
\*----------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------
** A driver with only one channel and small memory resources does not need
** the 'channel' parameter.
** The definition CP_SMALL_CODE is checked for the value '1' and the
** function prototypes are converted then. The function call in the
** application stays the same (with 'channel' parameter).
**
*/
#if   CP_SMALL_CODE == 1

#define  CpCoreAutobaud(CH, A, B)            CpCoreAutobaud(A, B)
#define  CpCoreBaudrate(CH, A)               CpCoreBaudrate(A)
#define  CpCoreBittiming(CH, A)              CpCoreBittiming(A)

#define  CpCoreBufferGetData(CH, A, B)       CpCoreBufferGetData(A, B)
#define  CpCoreBufferGetDlc(CH, A, B)        CpCoreBufferGetDlc(A, B)
#define  CpCoreBufferInit(CH, A, B, C)       CpCoreBufferInit(A, B, C)
#define  CpCoreBufferRelease(CH, A)          CpCoreBufferRelease(A)
#define  CpCoreBufferSetData(CH, A, B)       CpCoreBufferSetData(A, B)
#define  CpCoreBufferSetDlc(CH, A, B)        CpCoreBufferSetDlc(A, B)
#define  CpCoreBufferSend(CH, A)             CpCoreBufferSend(A)

#define  CpCoreCanMode(CH, A)                CpCoreCanMode(A)
#define  CpCoreCanState(CH, A)               CpCoreCanState(A)

#define  CpCoreFilterAll(CH, A)              CpCoreFilterAll(A)
#define  CpCoreFilterMsg(CH, A, B)           CpCoreFilterMsg(A, B)

#define  CpCoreHDI(CH, A)                    CpCoreHDI(A)

#define  CpCoreIntFunctions(CH, A, B, C)     CpCoreIntFunctions(A, B, C)

#define  CpCoreMsgRead(CH, A, B)             CpCoreMsgRead(A, B)
#define  CpCoreMsgWrite(CH, A, B)            CpCoreMsgWrite(A, B)

#define  CpCoreStatistic(CH, A)              CpCoreStatistic(A)

#endif


/*----------------------------------------------------------------------------*\
** Function prototypes                                                        **
**                                                                            **
\*----------------------------------------------------------------------------*/


/*!
** \brief   Run automatic bitrate detection
** \param   ptsPortV       Pointer to CAN port structure
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
**
*/
_TvCpStatus CpCoreAutobaud(_TsCpPort * ptsPortV, _U08 * pubBaudSelV, _U16 * puwWaitV);


/*!
** \brief   Set bitrate of CAN controller
** \param   ptsPortV       Pointer to CAN port structure
** \param   ubBaudSelV     Bitrate selection
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function directly writes to the bit timing registers of the CAN
** controller. The value for the parameter \e ubBaudSelV is taken
** from the CP_BAUD enumeration.
**
*/
_TvCpStatus CpCoreBaudrate(_TsCpPort * ptsPortV, _U08 ubBaudSelV);


/*!
** \brief   Set bitrate of CAN controller
** \param   ptsPortV       Pointer to CAN port structure
** \param   ptsBitrateV    Pointer to bit timing structure
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function directly writes to the bit timing registers of the CAN
** controller. Usage of the function requires a detailed knowledge of
** the used CAN controller hardware.
**
*/
_TvCpStatus CpCoreBittiming(_TsCpPort * ptsPortV, _TsCpBitTiming * ptsBitrateV);


/*!
** \brief   Get data from message buffer
** \param   ptsPortV       Pointer to CAN port structure
** \param   ubBufferIdxV   Buffer number
** \param   pubDataV       Buffer for data
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function is the fastest method to get data from a FullCAN message
** buffer. The buffer has to be configured by a call to CpCoreBufferInit() before.
**
*/
_TvCpStatus CpCoreBufferGetData( _TsCpPort * ptsPortV, _U08 ubBufferIdxV,
                                 _U08 * pubDataV);


/*!
** \brief   Get DLC of specified buffer
** \param   ptsPortV       Pointer to CAN port structure
** \param   ubBufferIdxV   Buffer number
** \param   pubDlcV        Data Length Code
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function retrieves the Data Length Code (DLC) of the selected buffer
** \c ubBufferIdxV.
*/
_TvCpStatus CpCoreBufferGetDlc(  _TsCpPort * ptsPortV, _U08 ubBufferIdxV,
                                 _U08 * pubDlcV);


/*!
** \brief   Initialise buffer in FullCAN controller
** \param   ptsPortV       Pointer to CAN port structure
** \param   ptsCanMsgV     Pointer to a CAN message structure
** \param   ubBufferIdxV   Buffer number
** \param   ubDirectionV   Direction of message
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** \see     CpCoreBufferRelease()
**
** This function allocates the message buffer in a FullCAN controller.
** The number of the message buffer inside the FullCAN controller is
** denoted via the parameter \c ubBufferIdxV.
** The parameter \c ubDirectionV can have the following values:
** \li CP_BUFFER_DIR_RX: receive
** \li CP_BUFFER_DIR_TX: transmit
**
** The following example shows the setup of a transmit buffer
** \dontinclude buf_init.c
** \skip    void AllocateTrmBuffer
** \until   }
**
** An allocated transmit buffer can be sent via the function
** CpCoreBufferSend().
*/
_TvCpStatus CpCoreBufferInit( _TsCpPort * ptsPortV, _TsCpCanMsg * ptsCanMsgV,
                              _U08 ubBufferIdxV, _U08 ubDirectionV);


/*!
** \brief   Release message buffer of FullCAN controller
** \param   ptsPortV       Pointer to CAN port structure
** \param   ubBufferIdxV   Buffer number
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** \see     CpCoreBufferInit()
*/
_TvCpStatus CpCoreBufferRelease( _TsCpPort * ptsPortV, _U08 ubBufferIdxV);


/*!
** \brief   Send message from message buffer
** \param   ptsPortV       Pointer to CAN port structure
** \param   ubBufferIdxV   Index of message buffer
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function transmits a message from the specified message buffer
** \c ubBufferIdxV. The message buffer has to be configured by a call to
** CpCoreBufferInit() in advance. The first message buffer starts at
** the index CP_BUFFER_1.
**
*/
_TvCpStatus CpCoreBufferSend(_TsCpPort * ptsPortV, _U08 ubBufferIdxV);


/*!
** \brief   Set data of message buffer
** \param   ptsPortV       Pointer to CAN port structure
** \param   ubBufferIdxV   Buffer number
** \param   pubDataV       Pointer to data buffer
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function is the fastest method to set the data bytes of a CAN message.
** It can be used in combination with the function CpCoreBufferSend(). It
** will write 8 data bytes into the buffer defined by \e ubBufferIdxV. The
** buffer has to be configured by CpCoreBufferInit() in advance. The size
** of the data buffer \e pubDataV must have a size of 8 bytes.
**
** The following example demonstrates the access to the data bytes of a CAN
** message:
** \code
**  _U08 aubDataT[8];   // buffer for 8 bytes
**
** aubDataT[0] = 0x11;  // byte 0: set to 11hex
** aubDataT[1] = 0x22;  // byte 1: set to 22hex

** //--- copy the stuff to message buffer 1 ---------------
** CpCoreBufferSetData(CP_CHANNEL_1, CP_BUFFER_1, &aubDataT);
**
** //--- send this message out ----------------------------
** CpCoreBufferSend(CP_CHANNEL_1, CP_BUFFER_1);
** \endcode
**
*/
_TvCpStatus CpCoreBufferSetData( _TsCpPort * ptsPortV, _U08 ubBufferIdxV,
                                 _U08 * pubDataV);


/*!
** \brief   Set DLC of specified buffer
** \param   ptsPortV       Pointer to CAN port structure
** \param   ubBufferIdxV   Buffer number
** \param   ubDlcV         Data Length Code
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function sets the Data Length Code (DLC) of the selected buffer
** ubBufferIdxV. The DLC must be in the range from 0 to 8.
*/
_TvCpStatus CpCoreBufferSetDlc(  _TsCpPort * ptsPortV, _U08 ubBufferIdxV,
                                 _U08 ubDlcV);



/*!
** \brief   Set state of CAN controller
** \param   ptsPortV       Pointer to CAN port structure
** \param   ubModeV        Mode selection
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function changes the operating mode of the CAN controller.
** Possible values for mode are defined in the #CP_MODE enumeration.
*/
_TvCpStatus CpCoreCanMode(_TsCpPort * ptsPortV, _U08 ubModeV);


/*!
** \brief   Retrieve status of CAN controller
** \param   ptsPortV       Pointer to CAN port structure
** \param   ptsStateV      Pointer to CAN state structure
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function retrieved the present state of the CAN controller. Possible
** values are defined in the #CP_STATE enumeration. The state of the CAN
** controller is copied to the variable pointer 'ptsStateV'.
*/
_TvCpStatus CpCoreCanState(_TsCpPort * ptsPortV, _TsCpState * ptsStateV);



//-------------------------------------------------------------------
// When the option CP_SMALL_CODE is set, the following function
// has no parameters. Inside the header file it must have the
// parameter type *void* then. The function is re-defined after-
// wards!
//
#if   CP_SMALL_CODE == 1
#define  CpCoreDriverInit(A, CH)             CpCoreDriverInit(void)
#endif
/*!
** \brief   Initialise the CAN driver
** \param   ubPhyIfV     CAN channel of the hardware
** \param   ptsPortV     Pointer to CAN port structure
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** \see     CpCoreDriverRelease()
**
** The functions opens the physical CAN interface defined by the
** parameter \c ubPhyIfV. The value for \c ubPhyIfV is taken from
** the enumeration CP_CHANNEL. The function sets up the field
** members of the CAN port handle \c ptsPortV. On success, the
** function returns CpErr_OK. On failure, the function can return
** the following values:
** <ul>
** <li>#CpErr_HARDWARE
** <li>#CpErr_INIT_FAIL
** </ul>
** An opened handle to a CAN port must be closed via the CpCoreDriverRelease()
** function.
*/
_TvCpStatus CpCoreDriverInit(_U08 ubPhyIfV, _TsCpPort * ptsPortV);
//-------------------------------------------------------------------
// Re-define the function for proper compilation.
//
#if   CP_SMALL_CODE == 1
#undef   CpCoreDriverInit
#define  CpCoreDriverInit(A, CH)             CpCoreDriverInit()
#endif



//-------------------------------------------------------------------
// When the option CP_SMALL_CODE is set, the following function
// has no parameters. Inside the header file it must have the
// parameter type *void* then. The function is re-defined after-
// wards!
//
#if   CP_SMALL_CODE == 1
#define  CpCoreDriverRelease(CH)             CpCoreDriverRelease(void)
#endif
/*!
** \brief   Release the CAN driver
** \param   ptsPortV       Pointer to CAN port structure
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** \see     CpCoreDriverInit()
**
** The implementation of this function is dependent on the operating
** system. Typical tasks might be:
** <ul>
** <li>clear the interrupt vector / routine
** <li>close all open paths to the hardware
** </ul>
**
*/
_TvCpStatus CpCoreDriverRelease(_TsCpPort * ptsPortV);
//-------------------------------------------------------------------
// Re-define the function for proper compilation.
//
#if   CP_SMALL_CODE == 1
#undef   CpCoreDriverRelease
#define  CpCoreDriverRelease(CH)             CpCoreDriverRelease()
#endif



/*!
** \brief   Get hardware description information
** \param   ptsPortV       Pointer to CAN port structure
** \param   ptsHdiV        Pointer to the Hardware Description Interface
**                         structure (_TsCpHdi)
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function retrieves information about the used hardware.
**
*/
_TvCpStatus CpCoreHDI(_TsCpPort * ptsPortV, _TsCpHdi * ptsHdiV);


/*!
** \brief   Install callback functions
** \param   ptsPortV       Pointer to CAN port structure
** \param   pfnRcvHandler  Pointer to callback function for receive interrupt
** \param   pfnTrmHandler  Pointer to callback function for transmit interrupt
** \param   pfnErrHandler  Pointer to callback function for error interrupt
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function will install three different callback routines in the
** interrupt handler. If you do not want to install a callback for a
** certain interrupt condition the parameter must be set to NULL.
** The callback functions for receive and transmit interrupt have the
** following syntax:
** <code>
** _U08 Handler(_TsCpCanMsg * ptsCanMsgV, _U08 ubBufferIdxV)
** </code>
** <p>
** The callback function for the CAN status-change / error interrupt has
** the following syntax:
** <code>
** _U08 Handler(_U08 ubStateV)
** </code>
** <p>
*/
_TvCpStatus CpCoreIntFunctions(  _TsCpPort * ptsPortV,
                                 _U08 (* pfnRcvHandler) (_TsCpCanMsg *, _U08),
                                 _U08 (* pfnTrmHandler) (_TsCpCanMsg *, _U08),
                                 _U08 (* pfnErrHandler) (_U08)                );


/*!
** \brief   Read a CAN message from controller
** \param   ptsPortV       Pointer to CAN port structure
** \param   ptsBufferV     Pointer to a CAN message structure
** \param   pulBufferSizeV Pointer to size variable
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function reads the receive queue from a CAN controller.
*/
_TvCpStatus CpCoreMsgRead( _TsCpPort * ptsPortV, _TsCpCanMsg * ptsBufferV,
                           _U32 * pulBufferSizeV);



/*!
** \brief   Transmit a CAN message
** \param   ptsPortV       Pointer to CAN port structure
** \param   ptsBufferV     Pointer to a CAN message structure
** \param   pulBufferSizeV Pointer to size variable
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function writes to the transmit queue of a CAN controller.
*/
_TvCpStatus CpCoreMsgWrite(_TsCpPort * ptsPortV, _TsCpCanMsg * ptsBufferV,
                           _U32 * pulBufferSizeV);


/*!
** \brief   Read CAN controller statistics
** \param   ptsPortV       Pointer to CAN port structure
** \param   ptsStatsV      Pointer to statistic data structure
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function copies CAN statistic information to the structure
** pointed by ptsStatsV.
**
*/
_TvCpStatus CpCoreStatistic(_TsCpPort * ptsPortV, _TsCpStatistic * ptsStatsV);


#endif   /* _CP_CORE_H_ */
