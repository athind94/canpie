//****************************************************************************//
// File:          cp_msg.h                                                    //
// Description:   CANpie message access functions                             //
// Author:        Uwe Koppe                                                   //
// e-mail:        koppe@microcontrol.net                                      //
//                                                                            //
// Copyright (C) MicroControl GmbH & Co. KG                                   //
// 53842 Troisdorf - Germany                                                  //
// www.microcontrol.net                                                       //
//                                                                            //
//----------------------------------------------------------------------------//
// Redistribution and use in source and binary forms, with or without         //
// modification, are permitted provided that the following conditions         //
// are met:                                                                   //
// 1. Redistributions of source code must retain the above copyright          //
//    notice, this list of conditions, the following disclaimer and           //
//    the referenced file 'COPYING'.                                          //
// 2. Redistributions in binary form must reproduce the above copyright       //
//    notice, this list of conditions and the following disclaimer in the     //
//    documentation and/or other materials provided with the distribution.    //
// 3. Neither the name of MicroControl nor the names of its contributors      //
//    may be used to endorse or promote products derived from this software   //
//    without specific prior written permission.                              //
//                                                                            //
// Provided that this notice is retained in full, this software may be        // 
// distributed under the terms of the GNU Lesser General Public License       //
// ("LGPL") version 3 as distributed in the 'COPYING' file.                   //
//                                                                            //
//----------------------------------------------------------------------------//
//                                                                            //
// Date        History                                                        //
// ----------  -------------------------------------------------------------- //
// 29.07.2003  Initial version                                                //
//                                                                            //
//****************************************************************************//


#ifndef  _CP_MSG_H_
#define  _CP_MSG_H_


//-----------------------------------------------------------------------------
// SVN  $Date: 2008-10-16 15:03:55 +0200 (Do, 16 Okt 2008) $
// SVN  $Rev: 148 $ --- $Author: microcontrol $
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
/*!
** \file    cp_msg.h
** \brief   CANpie message access
**
** &nbsp;<p>
** In order to create small and fast code, CANpie supplies a set of
** macros to access the CAN message structure (#_TsCpCanMsg). These
** macros can be used instead of the functions defined in the cp_msg.h
** header file. However keep in mind that macros can't be used to check
** for value ranges or parameter consistence. Usage of macros is enabled
** via the symbol #CP_CAN_MSG_MACRO.<p>
** \b Example
** \code
** //--- setup a CAN message ----------------------------------------
** _TsCpCanMsg   myMessage;
** CpMsgClear(&myMessage);                // clear the message
** CpMsgSetStdId(&myMessage, 100, 0);     // identifier is 100 dec, no RTR
** CpMsgSetDlc(&myMessage, 2);            // data length code is 2
** CpMsgSetData(&myMessage, 0, 0x11);     // byte 0 has the value 0x11
** CpMsgSetData(&myMessage, 1, 0x22);     // byte 1 has the value 0x22
** //... do something with it ....
**
** //--- evaluate a message that was received -----------------------
** _TsCpCanMsg   receiveMsg;
** //... receive the stuff ....
**
** if(CpMsgIsExtended(&receiveMsg))
** {
**    //--- this is an Extended Frame ---------------------
**    DoExtendedMessageService();
**    return;
** }
**
** if(CpMsgIsRemote(&receiveMsg))
** {
**    //... do something with RTR frames
** }
** \endcode
*/


/*----------------------------------------------------------------------------*\
** Include files                                                              **
**                                                                            **
\*----------------------------------------------------------------------------*/

#include "canpie.h"



/*----------------------------------------------------------------------------*\
** Definitions                                                                **
**                                                                            **
\*----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------*\
** Function prototypes                                                        **
**                                                                            **
\*----------------------------------------------------------------------------*/

/*!
** \brief   Clear RTR bit
** \param   ptsCanMsgV  Pointer to a _TsCpCanMsg message
** \see     CpMsgSetRemote()
**
** This function clears the Remote Transmission bit (RTR).
*/
void  CpMsgClrRemote(_TsCpCanMsg * ptsCanMsgV);

/*!
** \brief   Clear message structure
** \param   ptsCanMsgV  Pointer to a _TsCpCanMsg message
**
** This macro sets the identifier field and the flags field
** of a CAN message structure to 0. It is recommended to use
** this function when a message structure is assigned in memory.
*/
void  CpMsgClear(_TsCpCanMsg * ptsCanMsgV);


/*!
** \brief   Get Data
** \param   ptsCanMsgV  Pointer to a _TsCpCanMsg message
** \param   ubPosV      Zero based index of byte position
** \return  Value of data at position ubPosV
**
** This functions retrieves the data of a CAN message. The parameter
** \c ubPosV must be within the range 0 .. 7. Please note that the
** macro implementation does not check the value range of the parameter 
** \c ubPosV.
*/
_U08  CpMsgGetData(_TsCpCanMsg * ptsCanMsgV, _U08 ubPosV);



/*!
** \brief   Get Data Length Code
** \param   ptsCanMsgV  Pointer to a _TsCpCanMsg message
** \return  Data Length Code (DLC) of CAN message
**
** This function retrieves the data length code (DLC) of a CAN message.
** The return value range is between 0 and 8. 
*/
_U08  CpMsgGetDlc(_TsCpCanMsg * ptsCanMsgV);



/*!
** \brief   Get 29 Bit Identifier Value
** \param   ptsCanMsgV  Pointer to a _TsCpCanMsg message
** \return  Extended Identifier value
** \see     CpMsgGetStdId()
**
** This function retrieves the value for the identifier of an
** extended frame (CAN 2.0B). The frame format of the CAN message
** can be tested with the CpMsgIsExtended() function.
*/
_U32  CpMsgGetExtId(_TsCpCanMsg * ptsCanMsgV);


/*!
** \brief   Get 11 Bit Identifier Value
** \param   ptsCanMsgV  Pointer to a _TsCpCanMsg message
** \return  Standard Identifier value
** \see     CpMsgGetExtId()
** 
** This macro retrieves the value for the identifier of an
** standard frame (CAN 2.0A). The frame format of the CAN message
** can be tested with the CpMsgIsExtended() function.
*/
_U16  CpMsgGetStdId(_TsCpCanMsg * ptsCanMsgV);


/*!
** \brief   Check the frame type
** \param   ptsCanMsgV  Pointer to a _TsCpCanMsg message
** \return  0 for CAN 2.0A, 1 for CAN 2.0B
**
** This function checks the frame type. If the frame is CAN 2.0A
** (Standard Frame), the value 0 is returned. If the frame is
** CAN 2.0B (Extended Frame), the value 1 is returned.
*/
_U08  CpMsgIsExtended(_TsCpCanMsg * ptsCanMsgV);



/*!
** \brief   Check for remote frame
** \param   ptsCanMsgV  Pointer to a _TsCpCanMsg message
** \return  0 for data frame, 1 for remote frame
** 
** This function checks if the Remote Transmission bit (RTR) is
** set. If the RTR bit is set, the function will return 1, otherwise
** 0.
*/
_U08  CpMsgIsRemote(_TsCpCanMsg * ptsCanMsgV);


/*!
** \brief   Set Data
** \param   ptsCanMsgV  Pointer to a _TsCpCanMsg message
** \param   ubPosV      Zero based index of byte position
** \param   ubValueV    Value of data byte in CAN message
**
** This function sets the data in a CAN message. The parameter
** \c ubPosV must be within the range 0 .. 7. Please note that the
** macro implementation does not check the value range of the parameter 
** \c ubPosV.
*/
void  CpMsgSetData(_TsCpCanMsg * ptsCanMsgV, _U08 ubPosV, _U08 ubValueV);


/*!
** \brief   Set Data Length Code
** \param   ptsCanMsgV  Pointer to a _TsCpCanMsg message
** \param   ubDlcV      Data Length Code
**
** This function sets the Data Length Code (DLC) of a CAN message.
** The parameter ubDlcV must be within the range from 0..8.
** Please note that the macro implementation does not check the value
** range of the parameter \c ubDlcV.
*/
void  CpMsgSetDlc(_TsCpCanMsg * ptsCanMsgV, _U08 ubDlcV);


/*!
** \brief   Set 29 Bit Identifier Value
** \param   ptsCanMsgV     Pointer to a _TsCpCanMsg message
** \param   ulExtIdV       Identifier value
** \see     CpMsgSetStdFrame()
**
** This function sets the identifer value for an
** Extended Frame (CAN 2.0B) and marks the frame format as
** accordingly. The value of \c ulExtIdV is limited to
** #CP_MASK_EXT_FRAME.
*/
void  CpMsgSetExtId(_TsCpCanMsg * ptsCanMsgV, _U32 ulExtIdV);


/*!
** \brief   Set RTR bit
** \param   ptsCanMsgV  Pointer to a _TsCpCanMsg message
**
** This function sets the Remote Transmission bit (RTR).
*/
void  CpMsgSetRemote(_TsCpCanMsg * ptsCanMsgV);


/*!
** \brief   Set 11 Bit Identifier Value
** \param   ptsCanMsgV  Pointer to a _TsCpCanMsg message
** \param   uwStdIdV    Identifier value
** \see     CpMsgSetExtFrame()
**
** This function sets the identifer value for a Standard frame (CAN 2.0A).
** The value of \c uwStdIdV is limited to #CP_MASK_STD_FRAME.
*/
void  CpMsgSetStdId(_TsCpCanMsg * ptsCanMsgV, _U16 uwStdIdV);


/*!
** \brief   Set timestamp value
** \param   ptsCanMsgV  Pointer to a _TsCpCanMsg message
** \param   ptsTimeV    Pointer to timestamp structure
**
** This function sets the timestamp value for a CAN frame.
*/
void  CpMsgSetTime(_TsCpCanMsg * ptsCanMsgV, _TsCpTime * ptsTimeV);




//-------------------------------------------------------------------//
// Macros for CpMsgXXX() commands                                    //
//-------------------------------------------------------------------//
#if   CP_CAN_MSG_MACRO == 1

#define  CpMsgClrRemote(MSG_PTR)          ( (MSG_PTR)->ubMsgCtrl &= ~CP_MASK_RTR_BIT   );


#define  CpMsgClear(MSG_PTR)              ( (MSG_PTR)->tuMsgId.ulExt = 0); \
                                          ( (MSG_PTR)->ubMsgCtrl = 0);  \
                                          ( (MSG_PTR)->ubMsgDLC  = 0);

#define  CpMsgGetData(MSG_PTR, POS)       ( (MSG_PTR)->tuMsgData.aubByte[POS] )
#define  CpMsgGetDlc(MSG_PTR)             ( (MSG_PTR)->ubMsgDLC)

#define  CpMsgGetExtId(MSG_PTR)           ( (MSG_PTR)->tuMsgId.ulExt)
#define  CpMsgGetStdId(MSG_PTR)           ( (MSG_PTR)->tuMsgId.uwStd)

#define  CpMsgIsExtended(MSG_PTR)         ( (MSG_PTR)->ubMsgCtrl & CP_MASK_EXT_BIT )
#define  CpMsgIsRemote(MSG_PTR)           ( (MSG_PTR)->ubMsgCtrl & CP_MASK_RTR_BIT )
#define  CpMsgIsOverrun(MSG_PTR)          ( (MSG_PTR)->ubMsgCtrl & CP_MASK_OVR_BIT )

#define  CpMsgSetData(MSG_PTR, POS, VAL)  ( (MSG_PTR)->tuMsgData.aubByte[POS] = VAL )
#define  CpMsgSetDlc(MSG_PTR, DLC)        ( (MSG_PTR)->ubMsgDLC = DLC )

#define  CpMsgSetExtId(MSG_PTR, VAL)      ( (MSG_PTR)->tuMsgId.ulExt = VAL & CP_MASK_EXT_FRAME); \
                                          ( (MSG_PTR)->ubMsgCtrl |= CP_MASK_EXT_BIT   );

#define  CpMsgSetRemote(MSG_PTR)          ( (MSG_PTR)->ubMsgCtrl |= CP_MASK_RTR_BIT   );

#define  CpMsgSetOverrun(MSG_PTR)         ( (MSG_PTR)->ubMsgCtrl |= CP_MASK_OVR_BIT   );

#define  CpMsgSetStdId(MSG_PTR, VAL)      ( (MSG_PTR)->tuMsgId.uwStd = VAL & CP_MASK_STD_FRAME); \
                                          ( (MSG_PTR)->ubMsgCtrl &= ~CP_MASK_EXT_BIT   );
                           
#define  CpMsgSetTime(MSG_PTR, VAL)       ( (MSG_PTR)->tsMsgTime.ulSec1970 = (VAL)->ulSec1970); \
                                          ( (MSG_PTR)->tsMsgTime.ulNanoSec = (VAL)->ulNanoSec);

#endif


#endif   /* _CP_MSG_H_ */

