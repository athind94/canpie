//****************************************************************************//
// File:          cp_arch.h                                                   //
// Description:   CANpie architecture definitions                             //
// Author:        Uwe Koppe                                                   //
// e-mail:        koppe@microcontrol.net                                      //
//                                                                            //
// Copyright (C) MicroControl GmbH & Co. KG                                   //
// 53842 Troisdorf - Germany                                                  //
// www.microcontrol.net                                                       //
//                                                                            //
//----------------------------------------------------------------------------//
// Redistribution and use in source and binary forms, with or without         //
// modification, are permitted provided that the following conditions         //
// are met:                                                                   //
// 1. Redistributions of source code must retain the above copyright          //
//    notice, this list of conditions, the following disclaimer and           //
//    the referenced file 'COPYING'.                                          //
// 2. Redistributions in binary form must reproduce the above copyright       //
//    notice, this list of conditions and the following disclaimer in the     //
//    documentation and/or other materials provided with the distribution.    //
// 3. Neither the name of MicroControl nor the names of its contributors      //
//    may be used to endorse or promote products derived from this software   //
//    without specific prior written permission.                              //
//                                                                            //
// Provided that this notice is retained in full, this software may be        // 
// distributed under the terms of the GNU Lesser General Public License       //
// ("LGPL") version 3 as distributed in the 'COPYING' file.                   //
//                                                                            //
//----------------------------------------------------------------------------//
//                                                                            //
// Date        History                                                        //
// ----------  -------------------------------------------------------------- //
// 23.07.2003  Initial version                                                //
//                                                                            //
//****************************************************************************//


#ifndef  _CP_ARCH_H_
#define  _CP_ARCH_H_


//------------------------------------------------------------------------------
// SVN  $Date: 2009-02-28 18:46:35 +0100 (Sa, 28. Feb 2009) $
// SVN  $Rev: 151 $ --- $Author: microcontrol $
//------------------------------------------------------------------------------



//-----------------------------------------------------------------------------
/*!   \file    cp_arch.h
**    \brief   CANpie architecture definitions
**
**
*/


/*----------------------------------------------------------------------------*\
** Include files                                                              **
**                                                                            **
\*----------------------------------------------------------------------------*/
#include "compiler.h"      // compiler definitions
#include "cp_cc.h"         // definitions for CAN controller / target


/*----------------------------------------------------------------------------*\
** Definitions & Enumerations                                                 **
**                                                                            **
\*----------------------------------------------------------------------------*/

//-------------------------------------------------------------------
// The symbol CP_TARGET defines the target for the CANpie sources.
//
#define CP_TARGET CP_CC_LPC1549

#ifndef CP_TARGET
#error  Target (Symbol CP_TARGET) is not defined! Check file cp_cc.h!
#endif


/*----------------------------------------------------------------------------*\
** Structures                                                                 **
**                                                                            **
\*----------------------------------------------------------------------------*/




/*----------------------------------------------------------------------------*/
/*!
** \struct  CpPortLinux_s   cp_arch.h
** \brief   Port structure for Linux
**
*/
struct CpPortLinux_s {

   /*!   logical CAN interface number,
   **    first index is 0, value -1 denotes not assigned
   */
   int32_t  slLogIf;

   /*!   physical CAN interface number,
   **    first index is 0, value -1 denotes not assigned
   */
   int32_t  slPhyIf;

   /*!   CAN message queue number,
   **    first index is 0, value -1 denotes not assigned
   */
   int32_t  slQueue;
};


/*----------------------------------------------------------------------------*/
/*!
** \struct  CpPortEmbedded_s   cp_arch.h
** \brief   Port structure for embedded target
**
*/
struct CpPortEmbedded_s {

   /*!   physical CAN interface number,
   **    first CAN channel (index) is 0
   */
   _U08     ubPhyIf;

   /*!   logical CAN interface number,
   **    first index is 0
   */
   _U08     ubLogIf;

};


/*-------------------------------------------------------------------*/
/*!
** \def  CP_TARGET
** \ingroup CP_CONF
**
** This symbol defines target controller for the CANpie driver.
** The value can be set in a Makefile or in the cp_cc.h file.
**
** \internal This strange re-definition is only for doxygen.
*/
#if      CP_TARGET == 1
#undef   CP_TARGET         
#define  CP_TARGET   0
#endif


//---------------------------------------------------------------------
// Architecture definitions for Linux
//
#if CP_TARGET == CP_CC_LINUX

typedef struct CpPortLinux_s     _TsCpPort;
typedef int32_t                  _TvCpStatus;

#define CP_AUTOBAUD              0
#define CP_BUFFER_MAX            16
#define CP_CAN_MSG_MACRO         1
#define CP_CAN_MSG_USER          1
#define CP_CAN_MSG_TIME          1

#define CP_SMALL_CODE            0
#define CP_STATISTIC             1

#endif


//---------------------------------------------------------------------
// Architecture definitions for ATMEL CANary
//
#if CP_TARGET == CP_CC_CC01

typedef _U08                     _TsCpPort;
typedef _U08                     _TvCpStatus;

#define CP_AUTOBAUD              1
#define CP_BUFFER_MAX            14
#define CP_CAN_MSG_MACRO         1
#define CP_SMALL_CODE            1
#define CP_STATISTIC             0

#define CP_GLOBAL_RCV_ENABLE     0
#define CP_GLOBAL_RCV_BUFFER     16
#define CP_GLOBAL_RCV_MASK       0x00000000

#endif


//---------------------------------------------------------------------
// Architecture definitions for ATMEL AT90CAN128
//
#if CP_TARGET == CP_CC_AT90CAN128

typedef _U08                     _TsCpPort;
typedef _U08                     _TvCpStatus;

#define CP_AUTOBAUD              1
#define CP_BUFFER_MAX            14
#define CP_CAN_MSG_MACRO         1
#define CP_SMALL_CODE            1
#define CP_STATISTIC             0

#define CP_GLOBAL_RCV_ENABLE     0
#define CP_GLOBAL_RCV_BUFFER     16
#define CP_GLOBAL_RCV_MASK       0x00000000

#endif


//---------------------------------------------------------------------
// Architecture definitions for Microchip 18Fxx8x
//
#if CP_TARGET == CP_CC_18Fxx8x

typedef _U08                     _TsCpPort;
typedef _U08                     _TvCpStatus;

#define CP_AUTOBAUD              0
#define CP_BUFFER_MAX            12
#define CP_CAN_MSG_MACRO         1
#define CP_CAN_MSG_USER          1
#define CP_SMALL_CODE            1
#define CP_STATISTIC             0

#endif



//---------------------------------------------------------------------
// Architecture definitions for Fujitsu LX16 family (340)
//
#if CP_TARGET == CP_CC_LX340

typedef _U08               _TsCpPort;
typedef _U08               _TvCpStatus;

#define CP_AUTOBAUD        1
#define CP_BUFFER_MAX      16
#define CP_CAN_MSG_MACRO   1
#define CP_SMALL_CODE      1
#define CP_STATISTIC       1

#endif


//---------------------------------------------------------------------
// Architecture definitions for Fujitsu LX16 family (495)
//
#if CP_TARGET == CP_CC_LX495

typedef _U08                     _TsCpPort;
typedef _U08                     _TvCpStatus;

#define CP_AUTOBAUD              0
#define CP_BUFFER_MAX            8
#define CP_CAN_MSG_MACRO         1
#define CP_CAN_MSG_USER          1
#define CP_SMALL_CODE            1
#define CP_STATISTIC             0

#endif

//---------------------------------------------------------------------
// Architecture definitions for Infineon C164 family
//
#if CP_TARGET == CP_CC_C164

typedef _U08                     _TsCpPort;
typedef _U08                     _TvCpStatus;

#define CP_AUTOBAUD              0
#define CP_BUFFER_MAX            14
#define CP_CAN_MSG_MACRO         1
#define CP_SMALL_CODE            1
#define CP_STATISTIC             0

#define CP_GLOBAL_RCV_ENABLE     1
#define CP_GLOBAL_RCV_BUFFER     15
#define CP_GLOBAL_RCV_MASK       0x00000000

#endif


//---------------------------------------------------------------------
// Architecture definitions for Infineon XC164 family
//
#if CP_TARGET == CP_CC_XC164

typedef struct CpPortEmbedded_s  _TsCpPort;
typedef _U16                     _TvCpStatus;

#define CP_AUTOBAUD              0
#define CP_BUFFER_MAX            32
#define CP_CAN_MSG_MACRO         1
#define CP_SMALL_CODE            0
#define CP_STATISTIC             1

#define CP_GLOBAL_RCV_ENABLE     0
#define CP_GLOBAL_RCV_BUFFER     31
#define CP_GLOBAL_RCV_MASK       0x00000000

#endif


//---------------------------------------------------------------------
// Architecture definitions for Infineon C167 family
//
#if CP_TARGET == CP_CC_C167

typedef _U08               _TsCpPort;
typedef _U08               _TvCpStatus;

#define CP_AUTOBAUD        0
#define CP_BUFFER_MAX      16
#define CP_CAN_MSG_MACRO   1
#define CP_SMALL_CODE      1
#define CP_STATISTIC       0

#endif

//---------------------------------------------------------------------
// Architecture definitions for Freescale ColdFire MCF523x
//
#if CP_TARGET == CP_CC_MCF523x

typedef struct CpPortEmbedded_s  _TsCpPort;
typedef _U16                     _TvCpStatus;

#define CP_AUTOBAUD        		0
#define CP_BUFFER_MAX      		14
#define CP_CAN_MSG_MACRO   		1
#define CP_SMALL_CODE      		0
#define CP_STATISTIC       		1

#define CP_GLOBAL_RCV_ENABLE		1
#define CP_GLOBAL_RCV_BUFFER		16
#define CP_GLOBAL_RCV_MASK			0x00000000

#endif


//---------------------------------------------------------------------
// Architecture definitions for Freescale XGate
//
#if CP_TARGET == CP_CC_XGATE

typedef struct CpPortEmbedded_s  _TsCpPort;
typedef _U08                     _TvCpStatus;

#define CP_AUTOBAUD              1
#define CP_BUFFER_MAX            32
#define CP_CAN_MSG_MACRO         1
#define CP_CAN_MSG_USER          1
#define CP_SMALL_CODE            0
#define CP_STATISTIC             0

#define CP_GLOBAL_RCV_ENABLE     0
#define CP_GLOBAL_RCV_BUFFER     16
#define CP_GLOBAL_RCV_MASK       0x00000000

#endif


//---------------------------------------------------------------------
// Architecture definitions for Philips LPC21x9
//
#if (CP_TARGET == CP_CC_LPC2119) || (CP_TARGET == CP_CC_LPC2129)

typedef _U08               _TsCpPort;
typedef _U08               _TvCpStatus;

#define CP_SMALL_CODE      1
#define CP_CAN_MSG_MACRO   1
#define CP_CAN_MSG_USER    1
#endif


//---------------------------------------------------------------------
// Architecture definitions for ST STR71x
//
#if CP_TARGET == CP_CC_STR71x

typedef _U08               _TsCpPort;
typedef _U08               _TvCpStatus;

#define CP_SMALL_CODE      1
#define CP_CAN_MSG_MACRO   1
#endif

//---------------------------------------------------------------------
// Architecture definitions for ST STR91x
//
#if CP_TARGET == CP_CC_STR91x

typedef _U08                     _TsCpPort;
typedef _U08                     _TvCpStatus;

#define CP_AUTOBAUD              1
#define CP_BUFFER_MAX            32
#define CP_CAN_MSG_MACRO         1
#define CP_CAN_MSG_USER          0
#define CP_SMALL_CODE            1
#define CP_STATISTIC             1
#endif

//---------------------------------------------------------------------
// Architecture definitions for ST STM32F103x
//
#if CP_TARGET == CP_CC_STM32F103x

typedef _U08               _TsCpPort;
typedef _U08               _TvCpStatus;

#define CP_AUTOBAUD              1
#define CP_BUFFER_MAX            16
#define CP_CAN_MSG_MACRO         1
#define CP_CAN_MSG_USER          1
#define CP_SMALL_CODE            1
#define CP_STATISTIC             1

#endif


//---------------------------------------------------------------------
// Architecture definitions for ST ST10F2xx
//
#if CP_TARGET == CP_CC_ST10F2xx

typedef struct CpPortEmbedded_s  _TsCpPort;
typedef _U08                     _TvCpStatus;

#define CP_AUTOBAUD              1
#define CP_BUFFER_MAX            32
#define CP_CAN_MSG_MACRO         1
#define CP_CAN_MSG_USER          0
#define CP_CHANNEL_MAX           2
#define CP_SMALL_CODE            0
#define CP_STATISTIC             1

#define CP_GLOBAL_RCV_ENABLE     0
#define CP_GLOBAL_RCV_BUFFER     16
#define CP_GLOBAL_RCV_MASK       0x00000000

#endif

//---------------------------------------------------------------------
// Architecture definitions for Texas Instruments TMS320F2808
//
#if CP_TARGET == CP_CC_TMS320F2808

typedef struct CpPortEmbedded_s  _TsCpPort;
typedef _U16                     _TvCpStatus;

#define CP_AUTOBAUD              0
#define CP_BUFFER_MAX            32
#define CP_CAN_MSG_MACRO         1
#define CP_SMALL_CODE            0
#define CP_STATISTIC             1

#define CP_GLOBAL_RCV_ENABLE     0
#define CP_GLOBAL_RCV_BUFFER     31
#define CP_GLOBAL_RCV_MASK       0x00000000
#endif

//---------------------------------------------------------------------
// Architecture definitions for Silicon Laboratories C80551F040
//
#if CP_TARGET == CP_CC_C8051F040

typedef _U08                     _TsCpPort;
typedef _U08                     _TvCpStatus;

#define CP_AUTOBAUD              0
#define CP_BUFFER_MAX            32
#define CP_CAN_MSG_MACRO         1
#define CP_SMALL_CODE            1
#define CP_STATISTIC             1

#define CP_GLOBAL_RCV_ENABLE     0
#define CP_GLOBAL_RCV_BUFFER     16
#define CP_GLOBAL_RCV_MASK       0x00000000

#endif

//---------------------------------------------------------------------
// Architecture definitions for NXP LPC1549
//
#if CP_TARGET == CP_CC_LPC1549

typedef _U08                     _TsCpPort;
typedef _U08                     _TvCpStatus;

#define CP_AUTOBAUD              0
#define CP_BUFFER_MAX            32
#define CP_CAN_MSG_MACRO         1
#define CP_CAN_MSG_TIME			 0
#define CP_CAN_MSG_USER          0
#define CP_SMALL_CODE            1
#define CP_STATISTIC             1

#endif

#endif   /* _CP_ARCH_H_   */

