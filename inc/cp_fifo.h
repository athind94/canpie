//****************************************************************************//
// File:          cp_fifo.h                                                   //
// Description:   FIFO for CAN messages                                       //
// Author:        Uwe Koppe                                                   //
// e-mail:        koppe@microcontrol.net                                      //
//                                                                            //
// Copyright (C) MicroControl GmbH & Co. KG                                   //
// 53842 Troisdorf - Germany                                                  //
// www.microcontrol.net                                                       //
//                                                                            //
//----------------------------------------------------------------------------//
// Redistribution and use in source and binary forms, with or without         //
// modification, are permitted provided that the following conditions         //
// are met:                                                                   //
// 1. Redistributions of source code must retain the above copyright          //
//    notice, this list of conditions, the following disclaimer and           //
//    the referenced file 'COPYING'.                                          //
// 2. Redistributions in binary form must reproduce the above copyright       //
//    notice, this list of conditions and the following disclaimer in the     //
//    documentation and/or other materials provided with the distribution.    //
// 3. Neither the name of MicroControl nor the names of its contributors      //
//    may be used to endorse or promote products derived from this software   //
//    without specific prior written permission.                              //
//                                                                            //
// Provided that this notice is retained in full, this software may be        // 
// distributed under the terms of the GNU Lesser General Public License       //
// ("LGPL") version 3 as distributed in the 'COPYING' file.                   //
//                                                                            //
//----------------------------------------------------------------------------//
//                                                                            //
// Date        History                                                        //
// ----------  -------------------------------------------------------------- //
// 04.09.1999  Initial version                                                //
// 15.06.2000  added fixed FIFO size support                                  //
// 14.02.2001  complete rewrite, check FIFO status                            //
// 12.06.2007  rename file, change FIFO size to 32 bit                        //
//                                                                            //
//****************************************************************************//


#ifndef  _CP_FIFO_H_
#define  _CP_FIFO_H_


//------------------------------------------------------------------------------
// SVN  $Date: 2007-09-12 21:26:07 +0200 (Mit, 12 Sep 2007) $
// SVN  $Rev: 89 $ --- $Author: microcontrol $
//------------------------------------------------------------------------------



//-----------------------------------------------------------------------------
/*!   \file    cp_fifo.h
**    \brief   CANpie FIFO functions
**
**    The FIFO functions care for the handling of messages between the user
**    interface (user functions) and the specific hardware implementation.
**    The are currently two implementations for the FIFO: one with dynamic
**    memory allocation (using malloc() and free() functions) and a second
**    with static memory size. The latter is typically used for microcontroller
**    implementations with low memory resources. 
*/

/*----------------------------------------------------------------------------*\
** Include files                                                              **
**                                                                            **
\*----------------------------------------------------------------------------*/
#include "canpie.h"


/*----------------------------------------------------------------------------*\
** Definitions & Enumerations                                                 **
**                                                                            **
\*----------------------------------------------------------------------------*/
struct CpFifo_s {
   
   /*! This pointer is the first entry of an array of messages. The
   **  total number of messages is given by the field <b>uwFifoSize</b>.
   */
   _TsCpCanMsg *   ptsMsgList;
   
   /*! The 'uwFifoSize' field holds the number of maximum messages
   **  that can be stored in the FIFO.
   */
   _U32            ulFifoSize;
   
   _U32            ulHeadPos;
   _U32            ulTailPos;
};

typedef struct CpFifo_s   _TsCpFifo;





/*-------------------------------------------------------------------------
** Function prototypes
**
*/


/*!
** \brief   Setup FIFO
** \param   ptsFifoV      pointer to FIFO
**
** \return  Error code taken from the CpErr enumeration. If no error
**          occured, the function will return CpErr_OK.
**
** This function reserves memory for the specified FIFO buffer. If
** a static FIFO is used (CP_FIFO_TYPE == 1) the parameter 'ulSizeV'
** is not evaluated.
**
*/
_TvCpStatus CpFifoInit(_TsCpFifo * ptsFifoV, _U32 ulSizeV);


/*!
** \brief   Remove FIFO
** \param   ptsFifoV      pointer to FIFO
**
** \return  Error code taken from the CpErr enumeration. If no error
**          occured, the function will return CpErr_OK.
**
** This function frees the reserved memory for the specified FIFO
** buffer. If a static FIFO is used (CP_FIFO_TYPE == 1) this function
** simply returns CpErr_OK.
**
*/
_TvCpStatus CpFifoRelease(_TsCpFifo * ptsFifoV);



/*!
** \brief   Clear FIFO contents
** \param   ptsFifoV      pointer to FIFO
**
** This function clears the specified FIFO buffer. All messages inside
** the FIFO are erased.
**
*/
void CpFifoClear(_TsCpFifo * ptsFifoV );


/*!
** \brief   Check if FIFO is empty
** \param   ptsFifoV      pointer to FIFO
**
** \return  number of messages stored in FIFO
*/
_U32 CpFifoMsgCount(_TsCpFifo * ptsFifoV);


/*!
** \brief   Push message into FIFO
** \param   ptsFifoV      pointer to FIFO
** \param   ptsCanMsgV    pointer to CAN message
**
** \return  Error code taken from the CpErr enumeration. If no error
**          occured, the function will return CpErr_OK.
**
** Push a message into the FIFO buffer, given by the parameter 
** 'ptsFifoV'. If the buffer is full, the function will return
** 'CpErr_FIFO_FULL'.
**
*/
_TvCpStatus CpFifoPush(_TsCpFifo * ptsFifoV, _TsCpCanMsg * ptsCanMsgV);


/*!
** \brief   Pop message from FIFO
** \param   ptsFifoV      pointer to FIFO
** \param   ptsCanMsgV    pointer to CAN message
**
** \return  Error code taken from the CpErr enumeration. If no error
**          occured, the function will return CpErr_OK.
**
** Pop a message from the FIFO buffer, given by the parameter 
** 'ptsFifoV'. If the buffer is empty, the function will 
** return 'CpErr_FIFO_EMPTY'.
**
*/
_TvCpStatus CpFifoPop(_TsCpFifo * ptsFifoV, _TsCpCanMsg * ptsCanMsgV);


#endif   // _CP_FIFO_H_
