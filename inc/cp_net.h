//****************************************************************************//
// File:          cp_net.h                                                    //
// Description:   CANpie network functions                                    //
// Author:        Uwe Koppe                                                   //
// e-mail:        koppe@microcontrol.net                                      //
//                                                                            //
// Copyright (C) MicroControl GmbH & Co. KG                                   //
// 53842 Troisdorf - Germany                                                  //
// www.microcontrol.net                                                       //
//                                                                            //
//----------------------------------------------------------------------------//
// Redistribution and use in source and binary forms, with or without         //
// modification, are permitted provided that the following conditions         //
// are met:                                                                   //
// 1. Redistributions of source code must retain the above copyright          //
//    notice, this list of conditions, the following disclaimer and           //
//    the referenced file 'COPYING'.                                          //
// 2. Redistributions in binary form must reproduce the above copyright       //
//    notice, this list of conditions and the following disclaimer in the     //
//    documentation and/or other materials provided with the distribution.    //
// 3. Neither the name of MicroControl nor the names of its contributors      //
//    may be used to endorse or promote products derived from this software   //
//    without specific prior written permission.                              //
//                                                                            //
// Provided that this notice is retained in full, this software may be        //
// distributed under the terms of the GNU Lesser General Public License       //
// ("LGPL") version 3 as distributed in the 'COPYING' file.                   //
//                                                                            //
//----------------------------------------------------------------------------//
//                                                                            //
// Date        History                                                        //
// ----------  -------------------------------------------------------------- //
// 11.11.2008  Initial version                                                //
//                                                                            //
//****************************************************************************//


#ifndef  _CP_NET_H_
#define  _CP_NET_H_


//-----------------------------------------------------------------------------
// SVN  $Date: 2008-07-04 23:03:11 +0200 (Fr, 04 Jul 2008) $
// SVN  $Rev: 140 $ --- $Author: microcontrol $
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
/*!
** \file    cp_net.h
** \brief   CANpie network functions
**
** The network functions provide the */


/*----------------------------------------------------------------------------*\
** Include files                                                              **
**                                                                            **
\*----------------------------------------------------------------------------*/

#include "canpie.h"



/*----------------------------------------------------------------------------*\
** Definitions                                                                **
**                                                                            **
\*----------------------------------------------------------------------------*/




/*----------------------------------------------------------------------------*\
** Function prototypes                                                        **
**                                                                            **
\*----------------------------------------------------------------------------*/




/*!
** \brief   Set bitrate of CAN controller
** \param   ptsPortV       Pointer to CAN port structure
** \param   ubBaudSelV     Bitrate selection
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occured, the function will return \c CpErr_OK.
**
** This function directly writes to the bit timing registers of the CAN
** controller. The value for the parameter \e ubBaudSelV is taken
** from the CP_BAUD enumeration.
**
*/
_TvCpStatus CpNetBaudrate(_TsCpNet * ptsNetV, _U08 ubBaudSelV);






/*!
** \brief   Set state of CAN controller
** \param   ptsPortV       Pointer to CAN port structure
** \param   ubModeV        Mode selection
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function changes the operating mode of the CAN controller.
** Possible values for mode are defined in the #CP_MODE enumeration.
*/
_TvCpStatus CpNetCanMode(_TsCpNet * ptsNetV, _U08 ubModeV);


/*!
** \brief   Retrieve status of CAN controller
** \param   ptsPortV       Pointer to CAN port structure
** \param   ptsStateV      Pointer to CAN state structure
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** This function retrieved the present state of the CAN controller. Possible
** values are defined in the #CP_STATE enumeration. The state of the CAN
** controller is copied to the variable pointer 'ptsStateV'.
*/
_TvCpStatus CpNetCanState(_TsCpNet * ptsNetV, _TsCpState * ptsStateV);



/*!
** \brief   Initialize the CAN driver
** \param   ubPhyIfV     CAN channel of the hardware
** \param   ptsPortV     Pointer to CAN port structure
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occured, the function will return \c CpErr_OK.
**
** \see     CpCoreDriverRelease()
**
** The functions opens the physical CAN interface defined by the
** parameter \c ubPhyIfV. The value for \c ubPhyIfV is taken from
** the enumeration CP_CHANNEL. The function sets up the field
** members of the CAN port handle \c ptsPortV. On success, the
** function returns CpErr_OK. On failure, the function can return
** the following values:
** <ul>
** <li>#CpErr_HARDWARE
** <li>#CpErr_INIT_FAIL
** </ul>
** An opened handle to a CAN port must be closed via the CpNetClose()
** function.
*/
_TvCpStatus CpNetOpen(_U08 ubPhyIfV, _TsCpNet * ptsNetV);



/*!
** \brief   Release the CAN driver
** \param   ptsPortV       Pointer to CAN port structure
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occurred, the function will return \c CpErr_OK.
**
** \see     CpNetOpen()
**
** The implementation of this function is dependent on the operating
** system. Typical tasks might be:
** <ul>
** <li>clear the interrupt vector / routine
** <li>close all open paths to the hardware
** </ul>
**
*/
_TvCpStatus CpNetClose(_TsCpNet * ptsNetV);



/*!
** \brief   Get hardware description information
** \param   ptsPortV       Pointer to CAN port structure
** \param   ptsHdiV        Pointer to the Hardware Description Interface
**                         structure (_TsCpHdi)
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occured, the function will return \c CpErr_OK.
**
** This function retrieves information about the used hardware.
**
*/
_TvCpStatus CpNetInfo(_TsCpPort * ptsPortV, _TsCpHdi * ptsHdiV);




/*!
** \brief   Read a CAN message from controller
** \param   ptsPortV       Pointer to CAN port structure
** \param   ptsBufferV     Pointer to a CAN message structure
** \param   pulBufferSizeV Pointer to size variable
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occured, the function will return \c CpErr_OK.
**
** This function reads the receive queue from a CAN controller.
*/
_TvCpStatus CpNetMsgRead( _TsCpPort * ptsPortV, _TsCpCanMsg * ptsBufferV,
                           _U32 * pulBufferSizeV);



/*!
** \brief   Transmit a CAN message
** \param   ptsPortV       Pointer to CAN port structure
** \param   ptsBufferV     Pointer to a CAN message structure
** \param   pulBufferSizeV Pointer to size variable
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occured, the function will return \c CpErr_OK.
**
** This function writes to the transmit queue of a CAN controller.
*/
_TvCpStatus CpNetMsgWrite(_TsCpPort * ptsPortV, _TsCpCanMsg * ptsBufferV,
                           _U32 * pulBufferSizeV);


/*!
** \brief   Read CAN controller statistics
** \param   ptsPortV       Pointer to CAN port structure
** \param   ptsStatsV      Pointer to statistic data structure
**
** \return  Error code taken from the #CpErr enumeration. If no error
**          occured, the function will return \c CpErr_OK.
**
** This function copies CAN statistic information to the structure
** pointed by ptsStatsV.
**
*/
_TvCpStatus CpNetStatistic(_TsCpPort * ptsPortV, _TsCpStatistic * ptsStatsV);


#endif   /* _CP_CORE_H_ */
