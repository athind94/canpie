/*
 * lpc_helper.h
 *
 *  Created on: Aug 26, 2016
 *      Author: athi
 */

#ifndef LPC_HELPER_H_
#define LPC_HELPER_H_

void msgobj_to_cpmsg(CAN_MSG_OBJ *msgobj, _TsCpCanMsg *cpmsg);
void cpmsg_to_msgobj(_TsCpCanMsg *cpmsg, CAN_MSG_OBJ *msgobj);

inline _U08 buffer_busy(_U08 ubBufferIdxV) {
	return (((((LPC_CAN->TXREQ2) & (0x0000FFFF)) << 16) | ((LPC_CAN->TXREQ1) & (0x0000FFFF))) >> (ubBufferIdxV-1)) & 0x1;
}

#endif /* LPC_HELPER_H_ */
